type ProviderMetadata = {
    issuer: string,
    authorization_endpoint: string,
    token_endpoint: string,
    userinfo_endpoint?: string,
    jwks_uri: string,
    registration_endpoint?: string,
    scopes_supported?: string[],
    response_types_supported: string[],
    response_modes_supported?: string[],
    grant_types_supported?: string[]
    acr_values_supported?: string[],
    subject_types_supported: string[],
    id_token_signing_alg_values_supported: string[],
    id_token_encryption_alg_values_supported?: string[],
    id_token_encryption_enc_values_supported?: string[],
    userinfo_signing_alg_values_supported?: string[],
    userinfo_encryption_alg_values_supported?: string[],
    userinfo_encryption_enc_values_supported?: string[],
    request_object_signing_alg_values_supported?: string[],
    request_object_encryption_alg_values_supported?: string[],
    request_object_encryption_enc_values_supported?: string[],
    token_endpoint_auth_methods_supported?: string[],
    token_endpoint_auth_signing_alg_values_supported?: string[],
    display_values_supported?: string[],
    claim_types_supported?: string[],
    claims_supported?: string[],
    service_documentation?: string,
    claims_locales_supported?: string[],
    ui_locales_supported?: string[],
    claims_parameter_supported?: boolean,
    request_parameter_supported?: boolean,
    request_uri_parameter_supported?: boolean,
    require_request_uri_registration?: boolean,
    op_policy_uri?: string,
    op_tos_uri?: string
}

export async function fetchProviderMetadata(issuer_uri: string): Promise<ProviderMetadata> {
    //TODO: Add validation of configuration
    const providerMetadata: ProviderMetadata = await (await fetch(`${issuer_uri}/.well-known/openid-configuration`)).json()

    if (providerMetadata.scopes_supported && !providerMetadata.scopes_supported.includes('openid')) {
        throw Error("Invalid scopes_supported: does not include 'openid':" + providerMetadata.scopes_supported.toString())
    }

    if (!providerMetadata.response_types_supported.includes('code')) {
        throw Error("Invalid response_types_supported: does not include 'code':" + providerMetadata.response_types_supported)
    }

    return providerMetadata
}

// As defined by https://www.rfc-editor.org/rfc/rfc3986#section-2.3
const unreservedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-._~"

function getRandomState(length: number): string {
    const randomNumbers = new Uint8Array(length)
    window.crypto.getRandomValues(randomNumbers)

    const randomUnreserved = randomNumbers.map((number) => unreservedCharacters.charCodeAt(number % unreservedCharacters.length))

    return String.fromCharCode(...randomUnreserved)
}

async function getCodeChallenge(code_verifier: string) {
    const encoder = new TextEncoder()
    const hashBuffer = new Uint8Array(await window.crypto.subtle.digest("SHA-256", encoder.encode(code_verifier)))
    const base64String = btoa(String.fromCharCode(...hashBuffer))
    return base64String.replaceAll("=", "")
        .replaceAll("+", "-")
        .replaceAll("/", "_")
}

const STATE_KEY = "state"
const CURRENT_PATH_KEY = "current-path"
const NONCE_KEY = "nonce"
const CODE_VERIFIER_KEY = 'code-verifier'
const TOKEN_KEY = "token"

export async function redirectToAuthentication(parameters: AuthenticationRequestParameters, providerMetadata: ProviderMetadata) {
    const currentPath = window.location.pathname + window.location.search
    const state = getRandomState(40)
    const nonce = getRandomState(40)
    const code_verifier = getRandomState(120)
    const code_challenge = await getCodeChallenge(code_verifier)


    // Save for validation/redirects later
    localStorage.setItem(STATE_KEY, state)
    localStorage.setItem(CURRENT_PATH_KEY, currentPath)
    localStorage.setItem(NONCE_KEY, nonce)
    localStorage.setItem(CODE_VERIFIER_KEY, code_verifier)

    const authenticationRequestSearchParams = new URLSearchParams({
        scope: 'openid',
        response_type: 'code',
        client_id: parameters.client_id,
        redirect_uri: parameters.redirect_uri,
        state: state,
        nonce: nonce,
        code_challenge: code_challenge,
        code_challenge_method: 'S256'
    })

    window.location.assign(providerMetadata.authorization_endpoint + '?' + authenticationRequestSearchParams.toString())
}

type TokenResponse = {
    access_token: string
    token_type: string
    expires_in: number
    refresh_token: string
    scope?: string
}

export async function requestToken(code: string, state: string | null, parameters: AuthenticationRequestParameters, providerMetadata: ProviderMetadata): Promise<TokenResponse> {
    const storedState = localStorage.getItem(STATE_KEY)

    if (!storedState) {
        throw Error("State missing in localstorage")
    }

    if (state !== storedState) {
        throw Error("Mismatched stored state with state from response" + storedState + "!==" + state)
    }

    const code_verifier = localStorage.getItem(CODE_VERIFIER_KEY)

    if (!code_verifier) {
        throw Error("code_verifier is missing from localstorage")
    }

    const tokenRequestParameters = new URLSearchParams({
        grant_type: 'authorization_code',
        code: code,
        redirect_uri: parameters.redirect_uri,
        client_id: parameters.client_id,
        code_verifier: code_verifier
    })

    const response = await fetch(providerMetadata.token_endpoint, {
        method: 'POST',
        body: tokenRequestParameters,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        mode: 'cors'
    })

    const tokenResponse: TokenResponse = await response.json()

    const currentPath = localStorage.getItem(CURRENT_PATH_KEY)

    if (currentPath === null) {
        throw Error("Path to restore not present in localstorage")
    }

    window.location.replace(window.location.origin + currentPath)

    localStorage.removeItem(CURRENT_PATH_KEY)
    localStorage.removeItem(STATE_KEY)
    localStorage.removeItem(CODE_VERIFIER_KEY)
    localStorage.removeItem(NONCE_KEY)

    localStorage.setItem(TOKEN_KEY, JSON.stringify(tokenResponse))

    return tokenResponse
}

type AuthenticationRequestParameters = {
    client_id: string
    redirect_uri: string
    issuer_uri: string
}

export async function authenticate(parameters: AuthenticationRequestParameters): Promise<TokenResponse> {
    const storedStringTokenResponse = localStorage.getItem(TOKEN_KEY)
    if (storedStringTokenResponse) {
        return JSON.parse(storedStringTokenResponse)
    }

    const providerMetadata = await fetchProviderMetadata(parameters.issuer_uri)
    const search = new URLSearchParams(window.location.search)
    const code = search.get("code")

    if (code !== null) {
        return await requestToken(code, search.get('state'), parameters, providerMetadata)
    }
    else {
        await redirectToAuthentication(parameters, providerMetadata)
    }
    throw Error("Should never be here")
}