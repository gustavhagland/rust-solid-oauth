/* @refresh reload */
import { render } from 'solid-js/web';

import './index.css';
import App from './App';
import {authenticate} from "./oauth/oauth";

const root = document.getElementById('root');

if (import.meta.env.DEV && !(root instanceof HTMLElement)) {
  throw new Error(
    'Root element not found. Did you forget to add it to your index.html? Or maybe the id attribute got mispelled?',
  );
}

const tokenResponse = await authenticate({
  client_id: 'warp',
  redirect_uri: 'http://localhost:8080',
  issuer_uri: 'http://localhost:8081/realms/warp'
})

render(() => <App />, root!)
