use dotenvy::dotenv;
use rust_oauth_test::{run, Config};

#[tokio::main]
async fn main() {
    match dotenv() {
        Ok(path) => println!("Environment loaded from {:?}", path),
        Err(err) => println!(
            "Could not load .env file, using only standard environment: {:?}",
            err
        ),
    };

    let config = Config {
        addr: dotenvy::var("SERVE_ADDRESS").unwrap_or(String::from("127.0.0.1:8080")),
        static_dir: dotenvy::var("STATIC_DIR").unwrap_or(String::from("web-client/dist")),
    };

    run(config).await
}
