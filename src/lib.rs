use std::net::SocketAddr;
use std::path::Path;
use warp::{Filter, Rejection};

pub struct Config {
    pub addr: String,
    pub static_dir: String,
}

pub async fn run(config: Config) {
    let index_route = warp::get().and(warp::fs::file(
        Path::new(&config.static_dir).join("index.html"),
    ));

    let spa_routes = warp::fs::dir(config.static_dir);

    let api_routes = warp::path("api").and(authenticate()).map(|token| token);

    let address: SocketAddr = config.addr.parse().expect("Address must be valid");
    warp::serve(api_routes.or(spa_routes).or(index_route))
        .run(address)
        .await
}

fn authenticate() -> impl Filter<Extract = (String,), Error = Rejection> + Copy {
    let test = warp::header::<String>("Authentication").and_then(|token: String| async move {
        if token.is_empty() {
            Err(warp::reject())
        } else {
            Ok(token)
        }
    });

    return test;
}
